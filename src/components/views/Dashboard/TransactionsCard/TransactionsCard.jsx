import React, { Component } from 'react';

import './TransactionsCard.css';

class TransactionsCard extends Component {
    render() {
        const { transactionsInformation } = this.props;

        return (
            <div className="TransactionsCard-container">
                <div className="TransactionsCard-title">
                    Transactions History
                </div>
                <div className="TransactionsCard-entry"><b>Metadata:</b></div>
                <div className="TransactionsCard-entry">You have {transactionsInformation.length} transactions in your transaction history.</div>

                <br />
                <br />
            </div>
        );
    }
}

export default TransactionsCard;
