import React, { Component } from 'react';

import NavBar from './NavBar';
import CustomerCard from './CustomerCard';
import AccountCard from './AccountCard';
import TransactionsCard from './TransactionsCard';
import PaymentsCard from './PaymentsCard';
import RewardsCard from './RewardsCard';

import { getCustomerByCustomerIdPromise } from '../../../api/customers.js';
import { getAccountByAccountIdPromise } from '../../../api/accounts.js';
import { getTransactionsByCustomerIdPromise } from '../../../api/transactions.js';
import { getPaymentsByAccountIdPromise } from '../../../api/payments.js';
import { getRewardsByAccountIdPromise } from '../../../api/rewards.js';
import { getTotalRewardsRemainingByAccountIdPromise } from '../../../api/total_rewards_remaining.js';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: {},
            customer: {},
            transactionsByPrimaryCustomer: {},
            payments: {},
            rewards: {},
            totalRewardsRemaining: {}
        };

        this.parseQuery = this.parseQuery.bind(this);
    }

    componentDidMount() {
        const queryString = this.props.location.search;
        const parsed = this.parseQuery(queryString);

        // Get customer information, and pass it to child components.
        const customerPromise = getCustomerByCustomerIdPromise(parsed.customerId);
        customerPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    customer: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

        // Get account information, and pass it to child components.
        const accountPromise = getAccountByAccountIdPromise(parsed.accountId);
        accountPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    account: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

        // Get transactions by customer, and pass it to child components.
        const transactionsByCustomerPromise = getTransactionsByCustomerIdPromise(parsed.customerId);
        transactionsByCustomerPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    transactionsByPrimaryCustomer: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

        // Get payments by account, and pass it to child components.
        const paymentsByAccountPromise = getPaymentsByAccountIdPromise(parsed.accountId);
        paymentsByAccountPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    payments: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

        // Get payments by account, and pass it to child components.
        const rewardsByAccountPromise = getRewardsByAccountIdPromise(parsed.accountId);
        rewardsByAccountPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    rewards: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });

        // Get payments by account, and pass it to child components.
        const totalRewardsRemainingByAccountPromise = getTotalRewardsRemainingByAccountIdPromise(parsed.accountId);
        totalRewardsRemainingByAccountPromise
            .then((response) => {
                console.log(response.data);
                this.setState({
                    totalRewardsRemaining: response.data
                })
            })
            .catch((error) => {
                console.log(error);
            });
    }

    // Function copied from https://stackoverflow.com/questions/2090551/parse-query-string-in-javascript
    // Because I don't know why I can't import 'qs' or 'query-string'.
    parseQuery(queryString) {
        const query = {};
        const pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
        for (let i = 0; i < pairs.length; i++) {
            const pair = pairs[i].split('=');
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
        }
        return query;
    }

    render() {
        const { customer, account, transactionsByPrimaryCustomer, payments, rewards, totalRewardsRemaining } = this.state;

        return (
            <div>
                <NavBar customerFirstName={customer.first_name} />
                {
                    customer &&
                    <CustomerCard customerInformation={customer} />
                }
                {
                    (customer && account) &&
                    <AccountCard customerInformation={customer} accountInformation={account} />
                }
                {
                    transactionsByPrimaryCustomer &&
                    <TransactionsCard transactionsInformation={transactionsByPrimaryCustomer} />
                }
                {
                    payments &&
                    <PaymentsCard paymentsInformation={payments} />
                }
                {
                    (rewards && totalRewardsRemaining) &&
                    <RewardsCard rewardsInformation={rewards} totalRewardsRemainingInformation={totalRewardsRemaining} />
                }
                <br />
            </div>
        );
    }
}

export default Dashboard;
