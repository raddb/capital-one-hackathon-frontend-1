import React, { Component } from 'react';

import './Home.css';
import HomeCard from './HomeCard.jsx';

class Home extends Component {
  render() {
    return (
      <div className="HomeCard-centered">
        <HomeCard location={this.props.location} />
      </div>
    );
  }
}

export default Home;
